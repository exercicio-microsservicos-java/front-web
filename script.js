// Screen objects
const btnStartQuiz = document.querySelector("#startQuiz");
const btnSendAnswer = document.querySelector("#sendAnswer");
const questionCard = document.querySelector("#questionCard");
const txtPlayerName = document.querySelector("#playerName");
const questionPlaceholder = document.querySelector("#questionPlaceHolder");
const questionTitleIndex = document.querySelector("#questionTitleIndex");
const resultCard = document.querySelector("#resultCard");
const rankingCard = document.querySelector("#rankingCard");
const scorePlaceHolder = document.querySelector("#scorePlaceHolder");
const rankingPlaceHolder = document.querySelector("#rankingPlaceHolder");


const option1 =  document.querySelector("#lblAnswer1");
const option2 =  document.querySelector("#lblAnswer2");
const option3 =  document.querySelector("#lblAnswer3");
const option4 =  document.querySelector("#lblAnswer4");

const btnRestartGame = document.querySelector("#restartGame");
const btnSwitchUser = document.querySelector("#switchUser");
const btnViewRanking = document.querySelector("#viewRanking");
const btnRestartGame2 = document.querySelector("#restartGame2");
const btnSwitchUser2 = document.querySelector("#switchUser2");
const btnViewRanking2 = document.querySelector("#viewRanking2");

// Game API
const gameBaseURL = "http://localhost:8080/games";
const gameCreateEndpoint = "/create";
const viewRankingEndpoint = "/ranking";
function gameGetCurrentQuestionEndpoint(gameId) {
    return `/${gameId}/current-question`;
}
function gameAddAnswerEndpoint(gameId, answerIndex) {
    return `/${gameId}/add-answer/${answerIndex}`;
}
function gameSaveRankingEndpoint(gameId) {
    return `/${gameId}/save-ranking`;
}
function gameGetStateEndpoint(gameId) {
    return `/${gameId}/state`;
}

// Actions
function recoverState() {
    let gameId = localStorage.getItem("gameId");
    let gameState = localStorage.getItem("gameState");
    
    if (gameId != null) {        
        // btnStartQuiz.classList.add("d-none");
        // questionCard.classList.remove("d-none");
        // txtPlayerName.classList.add("d-none");
        // getCurrentQuestionAPICall(gameId);
        viewRankingAPICall();
    } else if (gameState === "GAME_FINISHED"){        
        gerarResultado();
    }
}

function startQuiz() {       
    createGameAPICall();    
}

function sendAnswer() {
    let answers = document.querySelectorAll("input[name='answers']");
    let answerIndex = 0;
    for (let i = 0; i < answers.length; i++) {
        const answer = answers[i];
        if (answer.checked) {
            answerIndex = i + 1;
        }
    }
    let gameId = localStorage.getItem("gameId");
    sendAnswerAPICall(gameId, answerIndex);
}

function gerarResultado() {
    resultCard.classList.remove("d-none");
    questionCard.classList.add("d-none");
    scorePlaceHolder.innerHTML = "Score: " + localStorage.getItem("score");
    let gameId = localStorage.getItem("gameId");
    saveRankingAPICall(gameId);
}

function restartGame() {
    startQuiz();
    resultCard.classList.add("d-none");
    rankingCard.classList.add("d-none");
}

function switchUser() {
    localStorage.clear;
    playerName.classList.remove("d-none");
    btnStartQuiz.classList.remove("d-none");
    resultCard.classList.add("d-none");
    rankingCard.classList.add("d-none");
}

function viewRanking() {
    resultCard.classList.add("d-none");
    rankingCard.classList.remove("d-none");
}

// API
function createGameAPICall() {
    let data = {
        "playerName": txtPlayerName.value
    };

    fetch(gameBaseURL + gameCreateEndpoint,
        {
            method: 'post',
            body: JSON.stringify(data),
            headers: {
                'content-type': 'application/json'
            }
        }).then((response) => {
            // nok
            if (!response.ok) {
                response.json().then(function (object) {
                    if (object.hasOwnProperty("mensagem")) {
                        alert(object.mensagem);
                    } else {
                        alert("Erro não esperado.");
                    }
                });
            // ok
            } else {
                response.json().then(function (object) {
                    localStorage.setItem("gameId", object.id);
                    localStorage.setItem("gameState", object.gameState);
                    localStorage.setItem("questionIndex", 1);
                    localStorage.setItem("score", 0);
                    getCurrentQuestionAPICall(localStorage.getItem("gameId"));
                    btnStartQuiz.classList.add("d-none");
                    questionCard.classList.remove("d-none");
                    txtPlayerName.classList.add("d-none");
                })
            }
        });
}

function getCurrentQuestionAPICall(gameId) {
    fetch(gameBaseURL + gameGetCurrentQuestionEndpoint(gameId),
        {
            method: 'get',
            headers: {
                'content-type': 'application/json'
            }
        }).then((response) => {
            // nok
            if (!response.ok) {
                response.json().then(function (object) {
                    if (object.hasOwnProperty("mensagem")) {
                        alert(object.mensagem);
                    } else {
                        alert("Erro não esperado.");
                    }
                });
            // ok
            } else {
                response.json().then(function (object) {
                    questionPlaceholder.innerHTML = object.title;
                    questionTitleIndex.innerHTML = `Pergunta ${localStorage.getItem("questionIndex")}`;
                    option1.innerHTML = object.option1;
                    option2.innerHTML = object.option2;
                    option3.innerHTML = object.option3;
                    option4.innerHTML = object.option4;
                })
            }
        });
}

function sendAnswerAPICall(gameId, answerIndex) {
    fetch(gameBaseURL + gameAddAnswerEndpoint(gameId, answerIndex),
        {
            method: 'get',
            headers: {
                'content-type': 'application/json'
            }
        }).then((response) => {
            // nok
            if (!response.ok) {
                response.json().then(function (object) {
                    if (object.hasOwnProperty("mensagem")) {
                        alert(object.mensagem);
                    } else {
                        alert("Erro não esperado.");
                    }
                });
            // ok
            } else {
                response.json().then(function (object) {
                    let questionIndex = localStorage.getItem("questionIndex");
                    questionIndex++;
                    localStorage.setItem("questionIndex", questionIndex);
                    localStorage.setItem("gameState", object.gameState);
                    localStorage.setItem("score", object.hits);
                    if (questionIndex > 10) {
                        gerarResultado();
                        return;
                    }
                    getCurrentQuestionAPICall(gameId);
                })
            }
        });
}

function gameStateAPICall(gameId) {
    fetch(gameBaseURL + gameGetStateEndpoint(gameId),
        {
            method: 'get',
            headers: {
                'content-type': 'application/json'
            }
        }).then((response) => {
            // nok
            if (!response.ok) {
                response.json().then(function (object) {
                    if (object.hasOwnProperty("mensagem")) {
                        alert(object.mensagem);
                    } else {
                        alert("Erro não esperado.");
                    }
                });
            // ok
            } else {
                response.json().then(function (object) {
                    // fazer alguma coisa
                })
            }
        });
}

function saveRankingAPICall(gameId) {
    fetch(gameBaseURL + gameSaveRankingEndpoint(gameId),
        {
            method: 'get',
            headers: {
                'content-type': 'application/json'
            }
        }).then((response) => {
            // nok
            if (!response.ok) {
                response.json().then(function (object) {
                    if (object.hasOwnProperty("mensagem")) {
                        alert(object.mensagem);
                    } else {
                        alert("Erro não esperado.");
                    }
                });
            // ok
            } else {
                //alert('Ranking gravado com sucesso');
            }
        });
}

function viewRankingAPICall() {
    fetch(gameBaseURL + viewRankingEndpoint,
        {
            method: 'get',
            headers: {
                'content-type': 'application/json'
            }
        }).then((response) => {
            // nok
            if (!response.ok) {
                response.json().then(function (object) {
                    if (object.hasOwnProperty("mensagem")) {
                        alert(object.mensagem);
                    } else {
                        alert("Erro não esperado.");
                    }
                });
            // ok
            } else {
                response.json().then(function (object) {
                    object.forEach(element => {
                        rankingPlaceHolder.innerHTML += `<p><strong>${element.usernameJogador}</strong>: ${element.pontuacao}</p>`;
                    });
                });
            }
        });
}

// Execute on page load

recoverState();

// Event hooks
btnStartQuiz.onclick = startQuiz;
btnSendAnswer.onclick = sendAnswer;
btnRestartGame.onclick = restartGame;
btnSwitchUser.onclick = switchUser;
btnViewRanking.onclick = viewRanking;
btnRestartGame2.onclick = restartGame;
btnSwitchUser2.onclick = switchUser;
btnViewRanking2.onclick = viewRanking;